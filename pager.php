<?php

if (! isset($_SERVER['HTTP_HOST'])) {
    exit('This script cannot be run from the CLI. Run it from a browser.');
}

if (! in_array(@$_SERVER['REMOTE_ADDR'], array(
    '127.0.0.1',
    '::1',
))
) {
    header('HTTP/1.0 403 Forbidden');
    exit('This script is only accessible from localhost.');
}

if (
    empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest'
) {
    header('HTTP/1.0 403 Forbidden');
    exit('Only ajax requests allowed.');
}

// Some handy definitions
define('ROOT', dirname(__FILE__) . '/');
define('CONFIG', ROOT . 'config/');
define('LIB', ROOT . 'lib/');

// Include configuration - don't forget to add your credentials.
include(CONFIG . 'database.php');

// Include a PSR-0 autoloader
include(LIB . 'autoloader.php');

// Main - over to you! :-)
use Local\Payment;

$page     = isset($_POST['page']) ? $_POST['page'] : 1;
$payments = Payment::findPage($page, Payment::NUMBER_PER_PAGE);
$results = [];

foreach ($payments as $payment) {
    $results[] = [
        'supplier'    => $payment->supplier,
        'cost_rating' => $payment->cost_rating,
        'ref'         => $payment->ref,
        'amount'      => $payment->amount,
    ];
}

$response = [
    'payments' => $results,
    'page'     => $page,
];

echo json_encode($response);