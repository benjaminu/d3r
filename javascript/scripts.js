jQuery(document).ready(function($) {
    var $tableHead = $('#table-head');
    var $table = $('#payments');
    var $pagination = $('#pagination');
    var url = $table.attr('data-url');
    var totalRows = parseInt($table.attr('data-totalRows'));
    var totalNumberOfPages = parseInt($table.attr('data-totalNumberOfPages'));
    var ratingMax = parseInt($table.attr('data-ratingMax'));
    var paginationPages = parseInt($table.attr('data-paginationPages'));

    $('.amount').each(function() {
        var $this = $(this);

        $this.html(accounting.formatMoney($this.html(), '£', 2));
    });

    addClickEvent();

    function addClickEvent() {
        $pagination.find('a').each(function () {
            $(this).on('click', function (e) {
                e.preventDefault();
                var $this = $(this);

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: { page : $this.attr('data-value') },
                    dataType: 'json'
                }).done(function(response) {
                    var payments = response.payments;
                    var paymentRows = buildRows(payments);

                    $tableHead.siblings().each(function () {
                        $(this).remove();
                    });

                    $table.append(paymentRows);

                    $pagination.html(pagination(parseInt(response.page)));
                    addClickEvent();
                });
            });
        });
    }

    function pagination(page) {
        var pager = '';

        if (page > 1) {
            pager += '<li><a href="/?page=' + (page - 1) + '" data-value="' + (page - 1) + '">';
            pager += '<span class="glyphicon glyphicon-menu-left"></span>';
            pager += '</a></li>';
        }

        var startPage = Math.ceil(Math.max(page - paginationPages/2, 1));
        var totalPages = Math.ceil(
            Math.max(1, Math.min(totalNumberOfPages, page + paginationPages/2))
        );

        for (var i = startPage; i < totalPages; i++) {
            pager += '<li';

            if (page == i) {
                pager += ' class="active"';
            }

            pager += '>';

            pager += '<a href="/?page=' + i + '" data-value="' + i + '">';
            pager += i;
            pager += '</a></li>';
        }

        if (page < totalNumberOfPages) {
            pager += '<li><a href="/?page=' + (page + 1) + '" data-value="' + (page + 1) + '">';
            pager += '<span class="glyphicon glyphicon-menu-right"></span>';
            pager += '</a></li>';
        }

        return pager;
    }

    function buildRows(payments) {
        var paymentRows = '';

        for (var i = 0; i < payments.length; i++) {
            paymentRows += '<tr>';
            paymentRows += '<td class="col-md-2">' + payments[i].supplier + '</td>';
            paymentRows += '<td><div class="ratings">';
            var rating = payments[i].cost_rating;
            for (var j = 0; j < rating; j++) {
                paymentRows += '<span class="rating on"></span>';
            }

            var unStarred = parseInt(ratingMax) - parseInt(rating);
            for (var j = 0; j < unStarred; j++) {
                paymentRows += '<span class="rating off"></span>';
            }

            paymentRows += '</div></td>';
            paymentRows += '<td class="col-md-1 text-center">' + payments[i].ref + '</td>';
            paymentRows += '<td class="col-md-1 text-center">' + accounting.formatMoney(payments[i].amount, '£', 2) + '</td>';
            paymentRows += '</tr>';
        }

        return paymentRows;
    }
});