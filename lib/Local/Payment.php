<?php

namespace Local;

use D3R\Model;
use D3R\Db;

/**
 * Class Payments
 *
 * @package Local
 */
class Payment extends Model
{
    /**
     * Maximum rating value.
     *
     * @var int
     */
    const RATING_MAX = 5;

    /**
     * Maximum number of rows to display per page.
     *
     * @var int
     */
    const NUMBER_PER_PAGE = 5;

    /**
     * Maximum number of rows to pagination links to display.
     *
     * @var int
     */
    const PAGINATION_PAGES = 5;

    /**
     * {@inherit}
     */
    protected static $_tableName = 'payments';

    /**
     * {@inherit}
     */
    protected static $_itemName = 'payment';

    /**
     * Returns all payment entries.
     *
     * @return array|bool
     * @throws \D3R\Exception
     */
    public static function findAll()
    {
        $sql = "SELECT * FROM " . static::tableName();

        if (false === ($result = Db::get()->select($sql, null))) {
            return false;
        }

        $models = array();
        if (0 < count($result)) {
            foreach ($result as $row) {
                $models[] = static::createFromArray($row);
            }
        }

        return $models;
    }

    /**
     * Returns total number of payment entries.
     *
     * @return integer|bool
     * @throws \D3R\Exception
     */
    public static function totalRows()
    {
        $sql = "SELECT COUNT(*) as total FROM " . static::tableName();

        if (false === ($result = Db::get()->select($sql, null))) {
            return false;
        }

        return $result[0]['total'];
    }
}