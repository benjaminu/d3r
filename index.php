<?php
/**
 * Basic front controller
 */

// Some handy definitions
define('ROOT', dirname(__FILE__) . '/');
define('CONFIG', ROOT . 'config/');
define('LIB', ROOT . 'lib/');

// Include configuration - don't forget to add your credentials.
include(CONFIG . 'database.php');

// Include a PSR-0 autoloader
include(LIB . 'autoloader.php');

// Main - over to you! :-)
use Local\Payment;

$page               = isset($_GET['page']) ? $_GET['page'] : 1;
$payments           = Payment::findPage($page, 5);
$totalRows          = Payment::totalRows();
$totalNumberOfPages = ceil($totalRows / Payment::NUMBER_PER_PAGE);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Supplier Payments - Chichester District Council</title>

    <!-- CSS -->
    <link href="css/min/styles.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<section class="container">
    <div class="row">
        <div class="col-md-12 page">
            <h1>Where your money goes</h1>
            <h2>Payments made by Chichester District Council to individual suppliers with a value over £500 made within
                October.</h2>
            <div class="search-box">
                <form class="form-inline">
                    <div class="form-group col-md-6 search-group no-padding">
                        <input type="search" class="form-control" id="search" placeholder="Search suppliers">
                    </div>
                    <div class="form-group col-md-3">
                        <select class="form-control col-md-12">
                            <option value="">Select from rating</option>
                            <?php for ($i = 1; $i <= Payment::RATING_MAX; $i++) : ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <button type="reset" class="btn btn-default btn-reset">Reset</button>
                    <button type="submit" class="btn btn-primary btn-search">Search</button>
                </form>
            </div>
            <div class="table-responsive">
                <table id="payments" class="table table-striped table-bordered table-curved"
                       data-url="/pager.php"
                       data-totalRows="<?php echo $totalRows;?>"
                       data-totalNumberOfPages="<?php echo $totalNumberOfPages;?>"
                       data-ratingMax="<?php echo Payment::RATING_MAX;?>"
                       data-paginationPages="<?php echo Payment::PAGINATION_PAGES; ?>"
                >
                    <tr id="table-head">
                        <th class="col-md-7">Supplier</th>
                        <th class="col-md-2 text-center">Pound Rating</th>
                        <th class="col-md-1 text-center">Reference</th>
                        <th class="col-md-1 text-center">Value</th>
                    </tr>
                    <?php foreach ($payments as $payment): ?>
                        <tr>
                            <td class="col-md-2"><?php echo $payment->field('supplier'); ?></td>
                            <td>
                                <div class="ratings">
                                    <?php $rating = $payment->field('cost_rating'); ?>
                                    <?php for ($i = 1; $i <= $rating; $i++): ?>
                                        <span class="rating on"></span>
                                    <?php endfor; ?>
                                    <?php $unStarred = Payment::RATING_MAX - (int)$rating; ?>
                                    <?php for ($i = 1; $i <= $unStarred; $i++): ?>
                                        <span class="rating off"></span>
                                    <?php endfor; ?>
                                </div>
                            </td>
                            <td class="col-md-1 text-center"><?php echo $payment->field('ref'); ?></td>
                            <td class="col-md-1 text-center amount"><?php echo $payment->field('amount'); ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <ul id="pagination" class="pagination">
                <?php if ($page > 1): ?>
                    <li>
                        <a href="/?page=<?php echo ($page - 1); ?>"
                           style="display: inline-block;"
                           data-value="<?php echo ($page - 1); ?>">
                            <span class="glyphicon glyphicon-menu-left"></span>
                        </a>
                    </li>
                <?php endif; ?>
                <?php $startPage  = ceil(max($page - Payment::PAGINATION_PAGES/2, 1));?>
                <?php $totalPages = ceil(max(1, min($totalNumberOfPages, $page + Payment::PAGINATION_PAGES/2)));?>
                <?php for ($i = $startPage; $i < $totalPages; $i++): ?>
                    <li <?php if ($page == $i) echo 'class="active"'; ?>>
                        <a href="/?page=<?php echo $i; ?>" data-value="<?php echo $i; ?>">
                            <?php echo $i; ?>
                        </a>
                    </li>
                <?php endfor; ?>
                <?php if ($page < $totalNumberOfPages): ?>
                    <li>
                        <a href="/?page=<?php echo ($page + 1); ?>"
                           style="display: inline-block;"
                           data-value="<?php echo ($page + 1); ?>">
                            <span class="glyphicon glyphicon-menu-right"></span>
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</section>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="vendors/jquery/dist/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendors/accounting/accounting.min.js"></script>
<script src="javascript/min/scripts.min.js"></script>
</body>
</html>