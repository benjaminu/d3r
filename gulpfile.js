var gulp = require('gulp');
var less = require('gulp-less');
var cleanCss = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var path = require('path');
var imagemin = require('gulp-imagemin');
var imageminPngquant = require('imagemin-pngquant');

var files = {
    less: path.join(__dirname, 'less/styles.less'),
    css: path.join(__dirname, 'css/*.css'),
    js: path.join(__dirname, 'javascript/*.js'),
    img: path.join(__dirname, 'img/**/*')
};

var dest = {
    css: path.join(__dirname, 'css'),
    css_min: path.join(__dirname, 'css/min'),
    js_min: path.join(__dirname, 'javascript/min'),
    img_min: path.join(__dirname, 'images')
};

gulp.task('less', function () {
    gulp.src(files.less)
        .pipe(less({
            errLogToConsole: true
        }))
        .pipe(gulp.dest(dest.css));
});

gulp.task('compress-css', function () {
    gulp.src(files.css)
        .pipe(sourcemaps.init())
        .pipe(cleanCss())
        .pipe(sourcemaps.write())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(dest.css_min));
});

gulp.task('compress-js', function () {
    return gulp.src(files.js)
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(dest.js_min));
});

gulp.task('compress-img', function () {
    return gulp.src(files.img)
        .pipe(imagemin({
            progressive: true,
            use: [imageminPngquant()]
        }))
        .pipe(gulp.dest(dest.img_min));
});

gulp.task('watch', function () {
    gulp.watch(files.less, ['less']);
    gulp.watch(files.css, ['compress-css']);
    gulp.watch(files.js, ['compress-js']);
    gulp.watch(files.img, ['compress-img']);
});

gulp.task('default', ['less', 'compress-css', 'compress-js', 'compress-img', 'watch']);